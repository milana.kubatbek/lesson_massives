void main(List<String> arguments) {
  //1: Выведите в конслоли превый, пятый и последний элемент списка.

  List list1 = [1, 7, 12, 3, 56, 2, 87, 34, 54];

  print(list1.first);
  print(list1[4]);
  print(list1.last);

//2: Объедените данные массивы и выведите в консоли,

  List list2 = [3, 12, 43, 1, 25, 6, 5, 7];
  list2.addAll([55, 11, 23, 56, 78, 1, 9]);

  print(list2);

  //3: необходимо вывести в консоли массив ['F','l','u','t','t','e','R']

  List list4 = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(list4.getRange(2, 9));

  //4: выведите true если массивы слодержит цифру 3 [1, 2, 3, 4, 5, 6, 7], также покажите первый и последний элемент массива и его длину

  List list5 = [1, 2, 3, 4, 5, 6, 7];

  print(list5.contains(3));
  print(list5.first);
  print(list5.last);
  print(list5.length);

//5:Попробуйте определить содержит ли список такие элементы как: 'dart' и 951

  List list6 = [601, 123, 2, "dart", 45, 95, "dart24", 1];
  print(list6.contains('dart'));
  print(list6.contains(951));

  //6: Попробуйте определить содержит ли список значение переменной myDart (Именно с большой буквы)

  List list7 = ['post', 1, 0, 'flutter'];
  String myDart = 'Flutter';
  print(list7.contains(myDart.toLowerCase()));

  //7: Объединить все элементы массива в одну строку и разделить каждое слово символом ‘*’ и сделать принт перменной myFlutter;

  List list8 = ['I', 'Started', 'Learn', 'Flutter', 'Since', 'April'];
  String myFlutter = '';

  print(list8.join('*'));

  //8: Провести сортировку массива, чтобы все элементы начинались по возрастающей. Вы должны получить в консоли:

  List<int> list9 = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];

  list9.sort(
    (a, b) => a.compareTo(b),
  );
  print(list9);
}
